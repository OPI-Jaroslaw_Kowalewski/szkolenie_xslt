<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<!-- ZADANIE -->

<!-- CZĘŚĆ I -->
<!--  
1. zbuduj stronę html z tabelą zawierającą:
 - wiersz nagłowka z nazwami kolumn z danych z xml-a
 - kolumnę Lp. jako pierwszą
 - kolejne pozycje z katalogu książek  
2. dodaj atrybut style="color: orange;" do wierszy nieparzystych
3. na dole tabeli dodaj wiersz z podsumowaniem, który:
 - w kolumnie autor wyświetli liczbę uniklanych autorów w katalogu
 - w kolumnie price wyświetli średnią cenę książki: hint średnia = suma / liczbę :)
 - w kolumnie data publikacji wyświetli datę wydania książki o pozycji nr 4  
 - w kolumnie genre wyświetl wszystkie gatunki występujące w katalogu
 - w kolumnie description wyświetl liczbę znaków w opisie dla przed ostatniej pozycji
 	hint: liczbę znaków liczymy poprzez funkcję string-length(xpath)
 -->

	<xsl:template match="/">
		
	</xsl:template>
	
</xsl:stylesheet>