<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:output indent="yes" method="xml"/>
<!-- 
1. osie Xpath
- http://homepage.cs.latrobe.edu.au/mjsutherland/WS/current/notes/lecture070_XSL_010.html
- 

 -->
	
	<xsl:template match="/">
		<html>
		<body>
			<h2>XPath: część I</h2>
			<ul>
				<li>Każde wyrażenie XPath ewaluuje się do <b>sekwencji</b> składającej się z zera lub większej liczby elementów (items). </li>
				<li>Elementem sekwencji może być <b>węzeł</b> lub <b>wartość atomowa</b>. Np. wyrażenie (1, 2.0, "C") wylicza się do trzyelementowej sekwencji wartości atomowych (każda innego typu).</li>
				<li>Zagnieżdżone sekwencje są automatycznie spłaszczane, np. (1, (2, 3)) jest równe (1, 2, 3).</li>
				<li>Wyrażenie XPath jest obliczane w kontekście, na który składają się m.in. dokument, węzeł bieżący.</li>
				<li>XPath jest typowany dynamicznie, dlatego błędy typowania zgłaszane są dopiero w trakcie obliczania wyrażenia. </li>
				<li>W przypadku leniwych obliczeń (obowiązkowe w if, opcjonalne w some i every) potencjalne błędy mogą nie zostać wykryte.</li>		
			</ul>
			<h3>Wartości atomowe</h3>
			<ul>
				<li>Wartość atomowa posiada typ prosty. np string, decimal. integer, date, etc</li>
				<li><a href="https://www.w3.org/TR/xpath-datamodel/#types-hierarchy">https://www.w3.org/TR/xpath-datamodel/#types-hierarchy</a></li>
			</ul>
			<h3>Węzły</h3>
			<ul>
				<li>dokument (korzeń dokumentu),</li>
				<li>element,</li>
				<li>atrybut,</li>
				<li>węzeł tekstowy,</li>
				<li>instrukcja przetwarzania,</li>
				<li>komentarz,</li>
				<li>węzeł przestrzeni nazw.</li>
			</ul>
			<h3>Wyrażenia XPath</h3>
			<ul>
				<li>Najczęściej używanymi wyrażeniami XPath są ścieżki. np. /kursy_walut/pozycja</li>
				<li>język ten pozwala na zapisywanie także wyrażeń arytmetycznych, logicznych i innych. np. /kursy_walut/pozycja[4]</li>
				<li>Oeratory arytmetyczne: +, -, *, div, idiv, mod</li>
				<li>Operatory logiczne: and i or</li>
				<li>union lub | - łączy zbiory</li>
				<li>intesect - przecięcie zbiorów oraz except - różnica zbiorów</li>
			</ul>
			<h3>Konstruktor sekwencji</h3>
			<ul>
				<li>Całą sekwencję obejmuje się nawiasami, a poszczególne elementy i przedziały rozdziela przecinkami.</li>
				<li>Sekwencje można jawnie tworzyć poprzez wyliczenie ich elementów lub podanie przedziałów</li>
				<li>Przykłady:
					<ul>
						<li>() – sekwencja pusta</li>
						<li>("Syncron") – równoważny wartości atomowej "Syncron"</li>
						<li>(1, 2,3, 4 ,6, 7, -3)</li>
						<li>(1 to 4, 6 to 7, -3)</li>
						<li>("ala", 20.5, 2 + 5, <b>//pozycja/nazwa</b>)</li>
						<li>(10, (1, 2), (), (3, 4)) – jest równe (10, 1, 2, 3, 4)</li>
					</ul>
				</li>
			</ul>
			<h3>Porównania</h3>
			<ul>
				<li>Do porównywania wartości atomowych służą operatory porównania atomowego: eq, ne, lt, le, gt i ge. lub encje &lt; &gt;</li>
				<li>Argumenty są <b>rzutowane na najwęższy wspólny typ</b>, a jeśli takiego nie ma lub jest nim xs:untypedAtomic, rzutowane na <b>xs:string</b>. </li>
				<li>Dla pewnych kombinacji typów niektóre operatory mogą nie być zdefiniowane, wówczas pojawia się błąd.</li>
				<li>Do porównywania sekwencji służą operatory porównania ogólnego: = != &lt; &lt;= > >=.</li>
				<li>Porównanie dla dwóch sekwencji L i R zwraca prawdę wtedy i tylko wtedy, gdy dla chociaż jednej pary: l (elementu L) oraz r (elementu R) zachodzi porównanie atomowe odpowiadające zadanemu porównaniu ogólnemu.</li>
				<li>
					<ul>
						<li>5 gt 3 – prawda</li>
						<li>(2) eq 2 – prawda</li>
						<li>(3) eq 2 – fałsz</li>
						<li>() eq 2 – pusta sekwencja</li>
						<li>(2, 3) eq 2 – błąd</li>
						<li>(2) = (2) – prawda</li>
						<li>2 = 2 – prawda</li>
						<li>() = 2 – fałsz</li>
						<li>(2, 3) = 2 – prawda</li>
						<li><b>(1, 2) = (2, 3) – prawda</b></li>
						<li>(2, 3) = (3, 4) – prawda</li>
						<li>(1, 2) = (3, 4) – fałsz (= nie jest przechodnie)</li>
						<li><b>(1, 2) != (1, 2) – prawda (_ != _ nie jest równoważne not(_ = _))</b></li>
						<li>//obiekt[9] = //obiekt[10] – prawda</li>
					</ul>			
				</li>
				
			</ul>
			<h3>Wyrażenie for</h3>
			<ul>
				<li>Obliczamy wyrażenie E1, jego wynikiem jest sekwencja.</li>
				<li>Dla każdego elementu Vi obliczonej sekwencji wykonujemy ewaluację wyrażenia E2 w kontekście wzbogaconym o zmienną x, której przypisano wartość równą Vi.</li>
				<li>Wynikiem jest sekwencja złożona z wyników poszczególnych ewaluacji, następnie "spłaszczona".</li>
				<li>
					<ul>
						<li>for $n in (10, 20, 30.5) return $n + 5</li>
						<li>for $x in //pozycja return concat("Pozycja o nazwie: ", $x/nazwa_waluty)</li>
						<li>for $x in X, $y in Y return $x + $y</li>
					</ul>
				</li>
			</ul>
			<h3>Funkcje Xpath</h3>
			<ul>
				<li>https://www.w3schools.com/xml/xsl_functions.asp</li>
				<li>http://saxon.sourceforge.net/saxon7.9.1/functions.html</li>
			</ul>
		</body>
		</html>
	
	</xsl:template>	
</xsl:stylesheet>