<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
 xmlns:xf="http://www.w3.org/2002/xforms"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/" >
	<html>
	<head></head>
	<body>
		<xsl:for-each select="//field">
			<xsl:element name="{@type}">
				<xsl:attribute name="bind" select="node"/>
				<xsl:element name="xf:label">
					<xsl:value-of select="label"/>
					
				</xsl:element>
			</xsl:element>		
		</xsl:for-each>
	</body>
	</html>
	</xsl:template>
	
</xsl:stylesheet>