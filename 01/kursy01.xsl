<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>
<!-- 
- wstęp do namespaceów
- formatowanie pliku wynikowego
- wersje xslt 1.0 i 2.0
- template główny - musi być zawsze
- xsl:for-each
- ewaluację wyrażeń w nawiasach klamrowych {}
- funkcje: concat, substring, replace, translate, upper-case, position, 
- zmienne 
- debugowanie: xsl:message
- SEKWENCJE!!!!
- if() then else 
- atrybut separator w xsl:value-of
- [] predykat, zwraca wartość logiczną, Jeśli true wybiera dany node jeśli false pomija
- union |
- //pozycja w xpath 1.0 wybiera pierwszy element, w xpath2.0 wybiera wszystkie


 -->
	<xsl:template match="/">
		<table border="1">
			<tr>
			<!-- 	<th><xsl:value-of 
				select="translate(//element/pozycja[position() = 1]/nazwa_waluty/name(),'_an',' *')"/></th> -->
				<!-- <th><xsl:value-of 
				select="concat(
					upper-case(substring(//element/pozycja[position() = 1]/nazwa_waluty/name(),1,1)),
					substring(replace(//element/pozycja[position() = 1]/nazwa_waluty/name(),'_',' '),2)
				)"/></th> -->
				<!-- <th><xsl:value-of 
				select="(
					upper-case(substring(//element/pozycja[position() = 1]/nazwa_waluty/name(),1,1)),
					substring(replace(//element/pozycja[position() = 1]/nazwa_waluty/name(),'_',' '),2)
				)" separator=""/></th>
				<th><xsl:value-of select="('napis',45, tabela_kursow/numer_tabeli, //kurs_sredni)"/></th> -->
				<xsl:variable name="pozycja_labelki" select="//element/pozycja[position() = 1]"/>
				<xsl:variable name="nazwa_waluty_labelka" select="(
					upper-case(substring($pozycja_labelki/nazwa_waluty/name(),1,1)),
					substring(replace($pozycja_labelki/nazwa_waluty/name(),'_',' '),2)
				)" />
				<th><xsl:value-of select="$nazwa_waluty_labelka" separator=""/></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			<!-- <xsl:for-each select="tabela_kursow/element/pozycja | tabela_kursow/pozycja"> -->
			<xsl:for-each select="//pozycja">
				<xsl:message terminate="no">
					<xsl:value-of select="('przetwarzam pozycję nr', position())"/>
				</xsl:message>
				<tr style="{if(position() mod 2 = 0) then 'color: green' else 'color: red;'}">
					<td><xsl:value-of select="nazwa_waluty"/></td>
					<td><xsl:value-of select="przelicznik"/></td>
					<td><xsl:value-of select="kod_waluty"/></td>
					<td><xsl:value-of select="kurs_sredni"/></td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>

