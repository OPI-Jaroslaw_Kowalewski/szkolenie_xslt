<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:my="jarek-uri-namespace" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>
<!-- 
- xsl:choose / when / otherwise
- własna funkcja
- zapamiętanie konteksu bieżącej iteracji w nadrzędnym repeat: contextPosition
- xsl:if
- * - nody w bieżącym kontekście, . = bieżący kontekst, 
- //* - wszystkie elementy od roota w dół - cały xml

 -->
	<xsl:template match="/">
		<table border="1">
			<xsl:for-each select="//pozycja[ position() > 8]">
				<xsl:variable name="contextPosition" select="position()"/>
				<xsl:message terminate="no">
					<xsl:value-of select="('przetwarzam pozycję nr', position())"/>
				</xsl:message>
				
				<tr>
					<xsl:for-each select="*">
						<xsl:choose>
							<xsl:when test="$contextPosition mod 10 = 1">
								<th><xsl:value-of select="my:prepareHeader(name())"/></th>
							</xsl:when>
							<xsl:when test="$contextPosition mod 5 = 1">
								<th><xsl:value-of select="'------------'"/></th>
							</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				
				<!-- <xsl:choose>
					<xsl:when test="position() mod 10 = 1">
						<tr>
							<xsl:for-each select="*">
								<th><xsl:value-of select="my:prepareHeader(name())"/></th>
							</xsl:for-each>
						</tr>
					</xsl:when>
					<xsl:when test="position() mod 5 = 1">
						<tr>
							<xsl:for-each select="*">
								<th><xsl:value-of select="''"/></th>
							</xsl:for-each>
						</tr>
					</xsl:when>
					<xsl:otherwise> </xsl:otherwise>
				</xsl:choose> -->
				
				<!-- <xsl:if test="position() mod 10 = 1">
					<tr>
						<xsl:for-each select="*">
							<th><xsl:value-of select="my:prepareHeader(name())"/></th>
						</xsl:for-each>
					</tr>
				</xsl:if>
				<xsl:if test="position() mod 5 = 1">
					<tr>
						<xsl:for-each select="*">
							<th><xsl:value-of select="''"/></th>
						</xsl:for-each>
					</tr>
				</xsl:if> -->
				<tr style="{if(position() mod 2 = 0) then 'color: green' else 'color: red;'}">
					<xsl:for-each select="*">
						<td>
							<xsl:value-of select="text()"/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	
	<xsl:function name="my:prepareHeader">
		<xsl:param name="nodeName"/>
		
		<xsl:value-of select="(
					upper-case(substring($nodeName,1,1)),
					substring(replace($nodeName,'_',' '),2)
					)" 
					separator=""/>
	</xsl:function>
</xsl:stylesheet>

