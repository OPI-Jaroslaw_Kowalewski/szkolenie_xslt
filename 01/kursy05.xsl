<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
xmlns:my="jarek-uri-namespace" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
>
<xsl:output indent="yes"/>

<!-- <xsl:variable name="headerCellType" select="'th'"/> -->
<xsl:param name="headerCellType" select="'th'"/>

<!-- 
- xsl:attribute, xsl:element, unikanie pustych atrybutów
- xsl:copy - kopiuje tylko bieżący element
- xsl:copy-of kopiuje bieżący element wraz z dziećmi
- dynamiczne tworzenie atrybutów i elementów
- zasięg zmiennych
- nadpistywanie wartości atrybutów
- dodawanie atrybutu MUSI się odbyć przed dodaniem dzieci/childów do danego elementu
- parametry arkusza stylu + pobieranie ich wartości z commandlina.
- java -jar ../saxon/saxon9.jar -xsl:kursy_jk.xsl -s:kursywalut.xml -o:kursy.html headerCellType=td
 -->
	<xsl:template match="/">
		<table border="1">
			<xsl:for-each select="//pozycja[ position() > 0 and not(@exclude='true')]">
				<xsl:sort select="translate(kurs_sredni, ',' , '.')" data-type="number"/>
				<xsl:variable name="contextPosition" select="position()"/>
				<tr>
					
					<!-- <xsl:if test="exists(./@class)">
						<xsl:attribute name="{@class/name()}" select="./@class"/>
					</xsl:if> -->
					<!-- <xsl:attribute name="style" select="'color: orange;'"/> -->
					<xsl:for-each select="./*">
						<xsl:choose>
							<xsl:when test="$contextPosition mod 10 = 1">
								<xsl:element name="{$headerCellType}">
									<xsl:value-of select="my:prepareHeader(name())"/>
								</xsl:element>
							</xsl:when>
							<xsl:when test="$contextPosition mod 5 = 1">
								<th><xsl:value-of select="'------------'"/></th>
							</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					
				</tr>
			
				<tr style="{if(position() mod 2 = 0) then 'color: green' else 'color: red;'}">
					<xsl:copy-of select="@*[name() = ('class','style')]"/>
					<xsl:for-each select="*">
						<td>
							<xsl:value-of select="text()"/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
				<tr>
					<th>
						<xsl:value-of 
						select="count(//pozycja[position() > 8 
							and starts-with(nazwa_waluty_test,'korona')])"/>
					</th>
					<th>
						<xsl:value-of 
							select="distinct-values(//pozycja[position() > 8]/przelicznik)"/>
					</th>
					<th>
						<xsl:value-of 
							select="avg(//pozycja[position() > 8]/przelicznik)"/>
					</th>
					<th>
						<xsl:value-of 
						select="sum(
						for $x in //pozycja[position() > 8]/kurs_sredni 
						return xs:decimal(translate($x,',','.')))"/>
					</th>
				</tr>
		</table>
	</xsl:template>
	
	<xsl:function name="my:prepareHeader">
		<xsl:param name="nodeName"/>
		
		<xsl:value-of select="(
					upper-case(substring($nodeName,1,1)),
					substring(replace($nodeName,'_',' '),2)
					)" 
					separator=""/>
	</xsl:function>
</xsl:stylesheet>

