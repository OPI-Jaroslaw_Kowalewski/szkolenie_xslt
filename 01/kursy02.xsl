<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:my="jarek-uri-namespace" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>
<!-- 

 -->
	<xsl:template match="/">
		<table border="1">
			<!-- <tr>
				<xsl:variable name="pozycja_labelki" select="//element/pozycja[position() = 1]"/>
			
				<th><xsl:value-of select="my:prepareHeader($pozycja_labelki/nazwa_waluty/name())"/></th>
				<th><xsl:value-of select="my:prepareHeader($pozycja_labelki/przelicznik/name())"/></th>
				<th><xsl:value-of select="my:prepareHeader($pozycja_labelki/kod_waluty/name())"/></th>
				<th><xsl:value-of select="my:prepareHeader($pozycja_labelki/kurs_sredni/name())"/></th>
			</tr> -->
			<xsl:for-each select="//pozycja[ position() > 8]">
				<xsl:message terminate="no">
					<xsl:value-of select="('przetwarzam pozycję nr', position())"/>
				</xsl:message>
				<xsl:if test="position() = 1">
					<!-- <tr>				
						<th><xsl:value-of select="my:prepareHeader(./nazwa_waluty/name())"/></th>
						<th><xsl:value-of select="my:prepareHeader(./przelicznik/name())"/></th>
						<th><xsl:value-of select="my:prepareHeader(./kod_waluty/name())"/></th>
						<th><xsl:value-of select="my:prepareHeader(./kurs_sredni/name())"/></th>
					</tr> -->
					<tr>
						<xsl:for-each select="*">
							<th><xsl:value-of select="my:prepareHeader(name())"/></th>
						</xsl:for-each>
					</tr>
				</xsl:if>
				<tr style="{if(position() mod 2 = 0) then 'color: green' else 'color: red;'}">
					<!-- <td><xsl:value-of select="nazwa_waluty"/></td>
					<td><xsl:value-of select="przelicznik"/></td>
					<td><xsl:value-of select="kod_waluty"/></td>
					<td><xsl:value-of select="kurs_sredni"/></td> -->
					<xsl:for-each select="*">
						<td>
							<xsl:value-of select="text()"/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	
	<xsl:function name="my:prepareHeader">
		<xsl:param name="nodeName"/>
		
		<xsl:value-of select="(
					upper-case(substring($nodeName,1,1)),
					substring(replace($nodeName,'_',' '),2)
					)" 
					separator=""/>
	</xsl:function>
</xsl:stylesheet>

